package com.cursospring.batch.jobexecutioncontext.processor;

import com.cursospring.batch.jobexecutioncontext.dto.EmployeeDTO;
import com.cursospring.batch.jobexecutioncontext.model.Employee;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import static com.cursospring.batch.jobexecutioncontext.utils.Constants.CUSTOM_CONTEXT_KEY;

@Component
@AllArgsConstructor
@Slf4j
public class EmployeeProcessor implements ItemProcessor<EmployeeDTO, Employee> {

    private final ExecutionContext executionContext;

    @Override
    public Employee process(EmployeeDTO dto) throws Exception {
        Employee employee = new Employee();
        employee.setEmployeeId(dto.getEmployeeId() + executionContext.getString(CUSTOM_CONTEXT_KEY));
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        employee.setAge(dto.getAge());
        log.info("Inside processor. Employee: {}", employee.toString());
        return employee;
    }
}