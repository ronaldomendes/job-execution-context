package com.cursospring.batch.jobexecutioncontext;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobexecutioncontext"})
public class JobExecutionContextApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobExecutionContextApplication.class, args);
    }

}
